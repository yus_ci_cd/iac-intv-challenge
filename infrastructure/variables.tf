variable "tags2" {
  type = map(string)
}

variable "oper_staging_bucket2" {
  type = string
}

variable "oper_terraform_state2" {
  type = string
}

variable "cluster_endpoint" {
  type = string
}

variable "python_webserver_image" {
  type = string
}

variable "login_token" {
  type = string
}

variable "registry_server" {
  type      = string
  sensitive = true
}

variable "registry_email" {
  type      = string
  sensitive = true
}

variable "registry_username" {
  type      = string
  sensitive = true
}
variable "registry_password" {
  type      = string
  sensitive = true
}
