resource "aws_s3_bucket" "oper_staging_bucket2" {
  bucket = var.oper_staging_bucket2
  acl    = "private"

  versioning {
    enabled = false
  }

  tags = var.tags2
}

resource "aws_s3_bucket" "oper_terraform_state2" {
  bucket = var.oper_terraform_state2
  acl    = "private"

  versioning {
    enabled = false
  }

  tags = var.tags2
}
