variable "tags2" {
  type = map(string)
}

variable "oper_staging_bucket2" {
  type = string
}

variable "oper_terraform_state2" {
  type = string
}
